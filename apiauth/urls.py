__author__ = 'tingsystems'

from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from oauth2_provider.views import AuthorizationView
from oauth.views import (
    UserListCreateView, UserRetrieveUpdateView, GroupListCreateView, GroupRetrieveUpdateView, GroupsToUserView,
    LoginView, PermissionsListCreateView, LogoutView, SiteListCreateView, SiteRetrieveUpdateView
)

auth_urls = [
    url(r'^authorize$', AuthorizationView.as_view(), name='authorize'),
    url(r'^token$', LoginView.as_view(), name='login'),
    url(r'^revoke-token$', LogoutView.as_view(), name='revoke-token'),
    url(r'^sites$', SiteListCreateView.as_view(), name='sites'),
    url(r'^sites/(?P<pk>[-\w]+)$', SiteRetrieveUpdateView.as_view(), name='sites-retrieve-update'),
    url(r'^users$', UserListCreateView.as_view(), name='users'),
    url(r'^users/(?P<pk>[-\w]+)$', UserRetrieveUpdateView.as_view(), name='users-retrieve-update'),
    url(r'^groups$', GroupListCreateView.as_view(), name='groups'),
    url(r'^groups/(?P<pk>[-\w]+)$', GroupRetrieveUpdateView.as_view(), name='groups-retrieve-update'),
    url(r'^user/groups$', GroupsToUserView.as_view(), name='user-groups-add'),
    url(r'^permissions$', PermissionsListCreateView.as_view(), name='permissions'),
]

urlpatterns = [
    url(r'^api/v{}/auth/'.format(settings.API_VERSION), include(auth_urls)),
    url(r'^ts/auth-backend/', admin.site.urls),
]
