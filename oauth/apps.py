__author__ = 'tingsystems'

from django.apps import AppConfig


class OauthConfig(AppConfig):
    name = 'oauth'
    verbose_name = 'Api auth'
