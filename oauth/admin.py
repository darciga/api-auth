__author__ = 'tingsystems'

from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from .models import User, Group, Permission, Site


@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    pass


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass

@admin.register(ContentType)
class ContentTypeAdmin(admin.ModelAdmin):
    pass

@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    pass


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    pass
